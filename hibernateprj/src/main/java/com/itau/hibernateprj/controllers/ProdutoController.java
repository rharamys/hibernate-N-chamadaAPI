package com.itau.hibernateprj.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.itau.hibernateprj.models.Produto;
import com.itau.hibernateprj.repositorios.ProdutoRepositorio;

@RestController
public class ProdutoController {
	
	@Autowired
	ProdutoRepositorio repositorioProduto;
	
	@RequestMapping(method=RequestMethod.GET, path="/produtos")
	public Iterable<Produto> getProdutos() {
		return repositorioProduto.findAll();
	}
	
	@RequestMapping(method=RequestMethod.POST, path="/produto")
	public Produto postProduto(@RequestBody Produto produto) {
		return repositorioProduto.save(produto);
	}
	
	@RequestMapping(method=RequestMethod.GET, path="/produto/{id}")
	public ResponseEntity<Produto> getProduto(@PathVariable long id) {
		Optional<Produto> optionalProduto = repositorioProduto.findById(id);
		if (!optionalProduto.isPresent()) {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();	
		} else {
			return ResponseEntity.status(HttpStatus.OK).body(optionalProduto.get());
		}
	}
	
	@RequestMapping(method=RequestMethod.PUT, path="/produto/{id}")
	public ResponseEntity<Produto> putProduto(@PathVariable long id, @RequestBody Produto produto) {
		Optional<Produto> optionalProduto = repositorioProduto.findById(id);
		if (!optionalProduto.isPresent()) {
			return ResponseEntity.status(HttpStatus.NO_CONTENT).build();	
		} else {
			optionalProduto.get().setNome(produto.getNome());
			optionalProduto.get().setPreco(produto.getPreco());
			Produto produtoSalvo = repositorioProduto.save(optionalProduto.get());
			return ResponseEntity.status(HttpStatus.OK).body(produtoSalvo);
		}	
	}
	
	@RequestMapping(method=RequestMethod.DELETE, path="/produto/{id}")
	public ResponseEntity<Produto> deleteProduto(@PathVariable long id) {
		Optional<Produto> optionalProduto = repositorioProduto.findById(id);
		if (!optionalProduto.isPresent()) {
			return ResponseEntity.noContent().build();
		} else {
			repositorioProduto.deleteById(id);
			return ResponseEntity.ok().build();
		}
	}

}