package com.itau.hibernateprj.controllers;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.itau.hibernateprj.models.Endereco;

@RestController
public class EnderecoController {

	RestTemplate restTemplate = new RestTemplate();
	
	@RequestMapping("/endereco/{cep}")
	public Endereco getEndereco(@PathVariable String cep) {
		String url = "https://viacep.com.br/ws/" + cep + "/json";
		return restTemplate.getForObject(url, Endereco.class);
	}
	
}
