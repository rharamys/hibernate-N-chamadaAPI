package com.itau.hibernateprj.repositorios;

import org.springframework.data.repository.CrudRepository;

import com.itau.hibernateprj.models.Produto;

public interface ProdutoRepositorio extends CrudRepository<Produto, Long> {

}
