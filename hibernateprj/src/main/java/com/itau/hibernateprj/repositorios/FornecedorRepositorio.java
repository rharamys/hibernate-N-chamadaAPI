package com.itau.hibernateprj.repositorios;

import org.springframework.data.repository.CrudRepository;

import com.itau.hibernateprj.models.Fornecedor;

public interface FornecedorRepositorio extends CrudRepository<Fornecedor, Long> {

	public Fornecedor findByEmail(String email);
}
